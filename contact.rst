Contact
=======

gRINN is developed and maintained primarily by `Onur Serçinoğlu <http://www.onursercinoglu.com>`_. For all technical issues/questions/requests/problems, please contact him directly at onursercin AT gmail DOT com. You may want to visit `this page <history.html>` to see a list of issues he's aware of and planned fixes/features for the upcoming versions of gRINN. 

For all scientific questions/discussions, contact the Principal Investigator (Dr. Pemra Ozbek) directly at pemra DOT ozbek AT marmara DOT edu DOT tr.

gRINN was part of Onur's work in `Pemra Ozbek <http://mimoza.marmara.edu.tr/~pemra.ozbek>`_'s lab at `Marmara University <http://www.marmara.edu.tr>`_ `Department of Bioengineering <http://bioe.eng.marmara.edu.tr>`_ `Computational Biology and Bioinformatics Research Group <http://compbio.bioe.eng.marmara.edu.tr>`_
