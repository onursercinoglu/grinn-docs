Issues and History/Change Log
============================

Known Issues - To-do List
^^^^^^^^^^^^^^^^^^^^^^^^^

Planned features/fixes (short-term)
-----------------------------------

* (**Fixed in v1.1.0.hf1)** gRINN fails to process GROMACS-generated TPR files in which no chain ID is present. This appears to be the case where only a single protein chain is included. This will be fixed in the next version.
* gRINN log file will include parameters used when calling gRINN.
* (**Fixed in v1.1.0.hf1)** gRINN fails to display IEMs and RCs for proteins with less than 100 amino acids. This will be fixed in the next version.
* gRINN will let the user export computed shortest paths list.
* Exported networks will include better residue labeling instead of simple integers.
* NAMD won't be spawned multiple times, letting the user give as input larger DCD files and avoid insufficient memory errors.
* Input file window will be simplified, avoiding the confusion on file types for NAMD/GROMACS data.
* Switching will be optional for NAMD-type data, as opposed to the current implementation (off)


Planned features/fixes (longer-term)
------------------------------------

* Support for non-protein residues (organic ligands, lipids, nucleic acids).
* Chain ID requirement on input files should be removed. We are working on code to relax this requirement.
* View Results UI will allow visualization of Electrostatic and vdW energies separately, in addition to separate network analysis features.
* A windows-compatible version is on the way.

Planned features/fixes (unlikely to implement in foreseeable future)
---------------------------------------------------------
* Support for AMBER-generated trajectories.

v1.1.0.hf1 (2018/06/21)
^^^^^^^^^^^^^^^^^^^^^^^

This hf (hot-fix) version fixes two bugs which rendered gRINN unusable in some cases and an addition to sample input files:

Bug fixes:

* A major bug in gRINN which leads to a failure in processing TPR files without chain IDs is corrected. gRINN will assign a default chain ID of "P" to residues which have no chain IDs assigned in input TPR.
* IEM annotation is now shown only for smallest proteins (with sizes of at most 20 amino acids).

Additions:

* charm27.ff files (used by GROMACS sample trajectory data) are included in the distribution (considering that this force-field may not be included in GROMACS installation of some users).


v1.1.0 (2018/04/06)
^^^^^^^^^^^^^^^^^^^

This version introduces a major internal code rehaul, leaving major features of gRINN unaffected.
There are additional new features as well as major/minor bug fixes:

New Features:

* A new calculation setting for non-bonded interaction cutoff for NAMD simulation input is introduced. In the previous version, filtering cutoff distance parameter specified both the filtering cutoff distance itself and the non-bonded interaction energy cutoff for NAMD simulation input.

* gRINN now supports Charmm simulation input as well.

Major/minor bug fixes:

* A bug which caused faulty reading of more than one parameter file for NAMD simulation input is fixed. 

* A minor bug which caused incorrect protein structure display upon start of View Results interface in Mac OS version is fixed.

v1.0.1 (2017/12/27)
^^^^^^^^^^^^^^^^^^^

Initial release of gRINN.
