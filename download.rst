Obtaining gRINN
===============

gRINN is free and open to all users.

Dependencies
------------

gRINN is designed to work with NAMD or GROMACS-generated MD simulation trajectories, hence *topology*, *structure* and *trajectory* files are required. 

Moreover, the tool interoperates with NAMD or GROMACS, so you will need to provide the *location of the NAMD or GROMACS(gmx) executable* you've used for your MD simulation before using gRINN. 

Since you're interested in using this tool, you're probably already a user of either NAMD or GROMACS; however, **if the executable is for some reason not installed/located on your system, gRINN will not work**. You should obtain and install them from the respective developers as we can't distribute them ourselves here. 

Obtaining NAMD
^^^^^^^^^^^^^^

**Please use a non-CUDA multicore version.**

**Please note that downloading NAMD requires registration**.

gRINN was tested against and confirmed to work fine with the following NAMD versions on Linux:

* NAMD 2.12b1, NAMD 2.12, NAMD 2.11, NAMD 2.10 and NAMD 2.9.

gRINN was tested against and confirmed to work fine with the following NAMD versions on Mac OSX High Sierra:

* NAMD 2.11 and 2.10.

**WARNING: NAMD 2.12 and NAMD 2.12b1 MacOSX versions lead to hang ups during gRINN operation. Please don't use these versions.**

Download NAMD `here <https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD>`_. 

Obtaining GROMACS
^^^^^^^^^^^^^^^^^

gRINN was tested against and confirmed to work fine with the following GROMACS versions on Linux:

* gromacs 2016.1, 2016.2, 2016.3, 2016.4, 5.1.2, 5.1.4.

**gRINN will surely not work with gromacs versions below 5.x and it may not be compatible with versions other than those listed above.**

Download GROMACS `here <http://www.gromacs.org/Downloads>`_. 

Download gRINN
--------------

Please read the `EULA <license.html>`_ first. By downloading and using gRINN, you agree to the terms and conditions contained therein. 

gRINN is available for both Linux (x64) and Mac OSX operating systems.

We recommend all users to download the latest version.

`Download gRINN v.1.1.0.hf1 for Linux v64 <https://www.dropbox.com/s/qnvoyjw7vrz8im4/grinn_linux_v110_hf1.tar.gz?dl=1>`_

`Download gRINN v.1.1.0.hf1 for Mac OSX <https://www.dropbox.com/s/baafeml8ll7evhp/grinn_macosx_v110_hf1.zip?dl=1>`_

`Download gRINN v.1.1.0 for Linux x64 <https://www.dropbox.com/s/rijb1em1q83b0kf/grinn_linux_v110.tar.gz?dl=1>`_

`Download gRINN v.1.1.0 for Mac OSX <https://www.dropbox.com/s/flywnhup8iedrwh/grinn_macosx_v110.zip?dl=1>`_

Windows support is currently a work-in-progress. Stay tuned and visit back for a Windows compatible executable.

Starting the application
------------------------

No installation is required.

Extract the archive you downloaded to a folder of your choice. Start a terminal in this folder and start the executable by typing::

	$ ./grinn

Please be patient for the application to start for the first time.

If you see the gRINN Main Window as shown below, you can continue with the `Tutorial <tutorial.html>`_.

.. image:: gRINN_mainWindow.png