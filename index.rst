.. gRINN documentation master file, created by
   sphinx-quickstart on Sat Dec 16 18:47:49 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gRINN's documentation!
=================================

gRINN is a software for residue interaction enery-based analysis of protein MD simulation trajectories.

Current version is v1.1.0.hf1.

Start with the `Tutorial <tutorial.html>`_. 

If you use gRINN for research or commercial purposes, please cite the following publication of gRINN in your publication (article, thesis, etc.):

.. [SO201] Onur Serçinoğlu, Pemra Ozbek; gRINN: a tool for calculation of residue interaction energies and protein energy network analysis of molecular dynamics simulations, Nucleic Acids Research, Volume 46, Issue W1, 2 July 2018, Pages W554–W562, https://doi.org/10.1093/nar/gky381

News
====

**2018/11/27**: Thank you for your interest in gRINN! We greatly appreciate your feedback. We're aware of several bugs, including incompatibility with some input data and gromacs versions, We're currently working to fix them in the next version release (anticipated January 2019). 
If you also experience problems, please feel free to send an email to onursercin AT gmail DOT com!

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction
   download
   tutorial
   miscellaneous
   history
   faq
   credits
   citing
   source
   contact
   license
         



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
