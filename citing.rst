Citing gRINN
========

If you use gRINN for research or commercial purposes, please cite the following publication of gRINN in your publication (article, thesis, etc.):

.. [SO2018] Onur Serçinoğlu, Pemra Ozbek; gRINN: a tool for calculation of residue interaction energies and protein energy network analysis of molecular dynamics simulations, Nucleic Acids Research, , gky381, https://doi.org/10.1093/nar/gky381
