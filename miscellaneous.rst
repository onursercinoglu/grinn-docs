Miscellaneous
========

Preparing NAMD/CHARMM input data for gRINN
------------------------------------------------------

gRINN has mainly two requirements in order to be able to process NAMD/CHARMM type simulation data efficiently: 

(1) "Dry" PSF/PDB/DCD files 

(2) PDB file with chain IDs assigned for all atoms.

Deleting non-protein atoms from PSF/PDB files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is fairly easy to remove non-protein atoms from input PDB/PSF/DCD files using VMD. 
Below we describe the steps in VMD (You need to have VMD installed on your computer).

* Start vmd from the directory in which your psf and pdb files are located by typing :code:`vmd` in a terminal.

* Save the following lines of Tcl code to a file named *make_dry_psf.tcl* into your current working directory.

.. code-block:: console

	mol load psf ionized.psf pdb ionized.pdb

	set a [atomselect top "not protein"]  
	set l [lsort -unique [$a get segid]]  
	
	package require psfgen
	readpsf ionized.psf
	coordpdb  ionized.pdb 

	foreach s $l {
	delatom $s
	}

	writepsf ionized_dry.psf
	writepdb ionized_dry.pdb

* Replace the *ionized.psf, ionized.pdb, ionized_dry.psf and ionized.pdb* with the file names of your psf/pdb and your desired output file names.

* Run the tcl script by starting the TkConsole from VMD Main Window (Extensions>TkConsole) and typing :code:`source make_dry_psf.tcl`.

* You should have obtained a psf/pdb file pair WITHOUT non-protein atoms now. Use them as input to gRINN.

.. note:: If you don't have chain IDs assigned in your PDB file, you will need to add chain IDs as well (see below).

Deleting non-protein atoms from DCD trajectory files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Start vmd from the directory in which your psf and pdb files are located by typing :code:`vmd` in a terminal.

* Load your psf file (e.g. ionized.psf) INCLUDING all atoms included in your simulation by choosing (File -> New Molecule) from VMD Main Window.

* Load your trajectory (dcd) onto this psf file by choosing (File -> New Molecule) and selecting "Load Files for..." option.

* Save a new trajectory file by choosing the molecule in Main Window, right-clicking on it and selection "Save Coordinates" option. Then, type *"protein"* to the "Selected Atoms" field and select "DCD" as the file-type. Click on "Save".

* Use this new DCD file as input to gRINN.

(Re)-assigning chain IDs back to the PDB file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A typical case for the PDB files that are prepared for NAMD/CHARMM simulations is that the chain IDs are deleted from the PDB and segment column is filled instead.

This is a problem for gRINN, since it distinguished between different residues by their chain IDs and residue numbers. If there is even a single atom with no chain IDs assigned in an input PDB file, gRINN will complain and refuse to process that file. 

It is possible to (re)-assign chain IDs back to PDB files by using any software with PDB manipulation capabilities. A simple option here would be the `PDBEditor <http://www.bioinformatics.org/pdbeditor/wiki/>`_ software. In order to change or add chain IDs to a PDB file, follow the steps listed below:

* Start PDBEditor.jar (you will need to have Java installed on your computer with necessary security permissions)

* Go to File -> Open Coordinate File, and select your PDB file.

* Go to Modify -> Edit Residue Number & Chain ID. 

* Type in a chain ID into the "New Chain ID" column. You should see that all other chain IDs are appropriately assigned.

* Click on "Apply Changes", then save your PDB file.

* Use this new PDB file as input to gRINN.

Deleting non-protein atoms from GROMACS input data
--------------------------------------------------

If you're submitting a GROMACS trajectory, there is no need for additional data preparation. You can just give your topology (TOP), run input (TPR) and trajectory (XTC or TRR) files as input to gRINN. However, a common practice with protein MD simulations is to keep only protein atoms and remove solvent/ions from trajectory files to save disk space unless solvent-protein interactions are the focus of a study. In this case, TPR and TOP files WITHOUT non-protein atoms should be prepared for gRINN. We describe here a step-by-step procedure to make TPR,TOP and TRR/XTC files that don't include any non-protein atoms.

Deleting non-protein atoms from TOP files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The only required action here is to delete lines containing non-protein atoms in the topology (TOP) file. For example, if your top file contains a certain number of water molecules, the end of the file might look like the following:

.. code-block:: console

	[ molecules ]
	; Compound        #mols
	Protein_chain_A     1
	Protein_chain_B     1
	Protein_chain_C     1
	SOL         24933
	NA               8

Simply delete the lines beginning with SOL and NA:

.. code-block:: console

	[ molecules ]
	; Compound        #mols
	Protein_chain_A     1
	Protein_chain_B     1
	Protein_chain_C     1

Save this file with a new file name (e.g. mytopol_dry.top) and use it as input to gRINN.

Deleting non-protein atoms from TPR files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The TPR file contains all information that is necessary to start an MD simulation with GROMACS. Making a new TPR file without any non-protein atoms is a bit more complicated than generating a "dry" topology file. The steps are described below:

* First, extract a PDB file from your TPR by calling **gmx editconf** in a terminal window:

.. code-block:: console

	gmx editconf -f mytpr.tpr -o mypdb.pdb

* Then, delete all non-protein atoms from this PDB file (using a suitable text edit, e.g. Sublime Text) and save it as e.g. "mypdb_dry.pdb".

* Next, modify your simulation input MDP file such that it does not contain any solvent/ion related parameters. In other words, prepare it as if you're going to run a simulation *in vacuo*. For example, if your MDP file contains such lines:

.. code-block:: console
	
	; Temperature coupling
	tcoupl		= V-rescale	            
	tc-grps		= Protein Non-Protein	
	ref_t		= 310 	  310	        

simply delete them. Then, save the file as e.g. "mymdp_dry.mdp".

* Next, create a new TPR file using your dry PDB and MDP files by calling **gmx grompp**:

.. code-block:: console

	gmx grompp -f mymdp_dry.mdp -c mypdb_dry.pdb -p mytopol_dry.top -o mymd_dry.tpr

* Use the resulting TPR as input to gRINN.